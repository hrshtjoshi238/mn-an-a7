let getElements=(str,arr)=>{
    if(str.indexOf("-")!=-1){
        let nums=str.split("-");
        return arr.filter((x)=>{
            return x.price>=parseInt(nums[0])&&x.price<=parseInt(nums[1])
        })
    }
    else{
        let nums=str.split(str.charAt(0));
        if(str.charAt(0)===">"){
            return arr.filter((x)=>{
                return x.price>parseInt(nums[1]);
            })  
        }
        else{
            return arr.filter((x)=>{
                return x.price<parseInt(nums[1]);
            })
        }
    }
}
const arr=[
 {code: "A101", price : 150},
 {code: "A452", price : 450},
 {code: "B671", price : 52},
 {code: "H887", price : 17},
 {code: "V693", price : 188},
 {code: "A645", price : 306},
 {code: "J034", price : 109},
 {code: "N299", price : 75},
 {code: "M472", price : 250},
 {code: "R077", price : 300},
 {code: "B297", price : 150},
 {code: "A489", price : 160},
 {code: "A507", price : 25},
 {code: "K563", price : 45},
 {code: "M833", price : 80},
 {code: "T672", price : 100},
 {code: "B934", price : 200}
]
console.log(getElements("30-85",arr));