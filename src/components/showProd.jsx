import React,{Component} from 'react';
class ShowProd extends Component{
    render(){
        let {product,index}=this.props;
        return(
            <div className="col-3 text-center border">
                <b>{product.code}</b><br/>
                Brand : {product.brand}<br/>
                Category : {product.category}<br/>
                Price : {product.price}<br/>
                Quantity : {product.quantity}<br/>
                Special Offer : {product.specialOffer?"Yes":"No"}<br/>
                Limited Stocks : {product.limitedStock?"Yes":"No"}<br/>
                <button className="btn btn-warning btn-sm"
                onClick={()=>this.props.get2Edit(index)}>Edit Details</button>
            </div>
        )
    }
}
export default ShowProd