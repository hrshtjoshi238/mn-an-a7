import React,{Component} from 'react';
class AddProduct extends Component{
state={
    product:this.props.product,
    categories:this.props.categories,
    brands:this.props.brands,
    editIndex:this.props.editIndex,
    errors:{}
    }
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        input.type==="checkbox"? 
            (s1.product[input.name]=input.checked)
            :(s1.product[input.name]=input.value);
        this.setState(s1);
    }
    validateAll=()=>{
        let {code,price,brand,category,specialOffer,limitedStock}=this.state.product;
        let errors={};
        errors.code=this.validateCode(code);
        errors.price=this.validatePrice(price);
        errors.brand=this.validateBrand(brand);
        errors.category=this.validateCategory(category);
        return errors;
    }
    validateCode=(code)=>{
        return !code?"Product Code must be entered":"";
    }
    validatePrice=(price)=>{
        return !price?"Price must be entered":"";
    }
    validateBrand=(brand)=>{
        return !brand?"Brand must be selected":"";
    }
    validateCategory=(category)=>{
        return !category?"Category must be selected":"";
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    submitProd=(e)=>{
        e.preventDefault();
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            if(errors.category)alert("Select the category");
            if(errors.brand)alert("Select the category");
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            this.props.onSubmit(this.state.product);
        }
    }
    render(){
        let {code,price,brand,category,specialOffer,limitedStock}=this.state.product;
        let {categories,brands,editIndex,errors}=this.state;
        return(
            <React.Fragment>
                <div className="form-group">
                    <label
                    htmlFor="code">Product Code</label>
                    <input
                    type="text"
                    className="form-control"
                    name="code"
                    disabled={editIndex>=0?"true":""}
                    value={code}
                    onChange={this.handleChange}/>
                </div>
                {errors.code?<div className="form-group" style={{background:"lightpink",color:"darkred",padding:"10px",borderRadius:"10px"}}>
                    {
                        errors.code
                    }
                </div>:""}
                <div className="form-group">
                    <label
                    htmlFor="code">Price</label>
                    <input
                    type="number"
                    className="form-control"
                    name="price"
                    value={price}
                    onChange={this.handleChange}/>
                </div>
                {errors.price?<div className="form-group" style={{background:"lightpink",color:"darkred",padding:"10px",borderRadius:"10px"}}>
                    {
                        errors.price
                    }
                </div>:""}
                <div className="form-group">
                    <label><b>Category</b></label><br/>
                    {
                categories.map((x)=>(
                    <span className="m-1">
                        <input 
                        type="radio" 
                        value={x} 
                        name="category"
                        id={x}
                        checked={category===x} 
                        onChange={this.handleChange}/>
                        <label className="ml-2" htmlFor={x}>{x}</label>
                    </span>
                ))
            }
                </div>
                <div className="form-group">
                    <select className="form-control" name="brand" value={brand} onChange={this.handleChange}>
                        <option disabled selected value="">Select Brand</option>
                        {
                            brands.map((x)=><option>{x}</option>)
                        }
                    </select>
                </div>
                <div className="form-group">
                    <label><b>Choose other info about the product</b></label>
                </div>
                <div className="form-group">
                    <input type="checkbox"
                    className="m-2"
                    id="specialOffer"
                    value={specialOffer}
                    name="specialOffer"
                    checked={specialOffer}
                    onChange={this.handleChange}/>
                    <label htmlFor="specialOffer">Special Offer</label>
                </div>
                <div className="form-group">
                    <input type="checkbox"
                    className="m-2"
                    id="limitedStock"
                    value={limitedStock}
                    name="limitedStock"
                    checked={limitedStock}
                    onChange={this.handleChange}/>
                    <label htmlFor="limitedStock">Limited Stock</label>
                </div>
                <div className="form-group">
                    <button className="btn btn-primary mb-4"
                    onClick={this.submitProd}>{editIndex>=0?"Edit Product":"Add Product"}</button><br/>
                    <button className="btn btn-primary"
                    onClick={()=>this.props.back2Home(0)}>Get Back to Home Page</button>
                </div>
            </React.Fragment>
        )
    }

}
export default AddProduct;