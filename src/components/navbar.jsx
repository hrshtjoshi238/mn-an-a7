import React, {Component} from 'react';
class NavBar extends Component{
    render(){
        let {products}=this.props;
        let totalProd=products.length;
        let quantity=products.reduce((acc,curr)=>acc+=curr.quantity,0);
        let value=products.reduce((acc,curr)=>acc+=curr.quantity*curr.price,0);
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="#">
                    ProdStoreSystem
                </a>
                <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-taget="#navbarSupportedContent">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto" href="#">
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Products
                                <span className="badge badge-pill badge-primary">
                                    {totalProd}
                                </span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Quantity
                                <span className="badge badge-pill badge-primary">
                                    {quantity}
                                </span>
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Value
                                <span className="badge badge-pill badge-primary">
                                    {value}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}
export default NavBar;