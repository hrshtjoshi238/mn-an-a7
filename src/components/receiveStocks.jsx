import React,{Component} from 'react';
class ReceiveStocks extends Component{
    state={
        product:this.props.product,
        codes:this.props.codes,
        years:this.props.years,
        months:this.props.months,
        errors:{}
    }
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        if(input.name==="year"||input.name==="month"||input.name==="day"){
            s1.product.date[input.name]=input.value;
        }
        else{
        s1.product[input.name]=input.value;
        }
        this.setState(s1);
    }
    submitProd=(e)=>{
        e.preventDefault();
        let {product}=this.state;
        let {code,quantity,date}=this.state.product;
        let {year,month,day}=date;
        if(!code){
            alert("Code is not Selected");
        }
        else if(!quantity){
            alert("Quantity is not Selected");
        }
        else if(!day){
            alert("Date is not Selected");
        }
        else{this.props.handleStockSubmit(this.state.product);}
    }
    getDays=(year,month)=>{
        let {months}=this.state;
        let days=[];
        let leap=false;
        if(month!="February"){
            if(month==="July"||month==="August"){
                for(let i=1;i<=31;i++){
                    days.push(i);
                }
            }
            else{
                let index=months.findIndex((x)=>x===month);
                if(index<=5){
                    if ((index+1)%2!=0){
                        for(let i=1;i<=31;i++){
                            days.push(i);
                        }
                    }
                    else{
                        for(let i=1;i<=30;i++){
                            days.push(i);
                        }
                    }
                }
                else if(index>7){
                    if ((index+1)%2!=0){
                        for(let i=1;i<=30;i++){
                            days.push(i);
                        }
                    }
                    else{
                        for(let i=1;i<=31;i++){
                            days.push(i);
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(year)%400===0){
                leap=true;
            }
            else if(parseInt(year)%100===0){
                leap=false;
            }
            else if(parseInt(year)%4===0){
                leap=true;
            }
            else{
                leap=false;
            }
            if(leap===false){
                for(let i=1;i<=28;i++){
                    days.push(i);
                }
            }
            else{
                for(let i=1;i<=29;i++){
                    days.push(i);
                }
            }
        }
        return days;
    }
    render(){
        let {code,quantity,date}=this.state.product;
        let {year,month,day}=date;
        let {codes,years,months}=this.state;
        let days=this.getDays(year,month);
        return(
            <React.Fragment>
                <h3>Select the product whoes stock has been received</h3>
                <div className="form-group">
                    <select className="form-control" name="code" value={code} onChange={this.handleChange}>
                        <option disabled selected value="">Select Code</option>
                        {
                            codes.map((x)=><option>{x}</option>)
                        }
                    </select>
                </div>
                <div className="form-group">
                    <label
                    htmlFor="code">Stocks Received</label>
                    <input
                    type="number"
                    className="form-control"
                    name="quantity"
                    value={quantity}
                    onChange={this.handleChange}/>
                </div>
                <div className="form-group d-flex">
                    <select className="form-control mr-2" name="year" value={year} onChange={this.handleChange}>
                        <option disabled selected value="">Select Year</option>
                        {
                            years.map((x)=><option>{x}</option>)
                        }
                    </select>
                    <select className="form-control mr-2" name="month" value={month} onChange={this.handleChange}>
                        <option disabled selected value="">Select Month</option>
                        {
                            months.map((x)=><option>{x}</option>)
                        }
                    </select>
                    <select className="form-control mr-2" name="day" value={day} onChange={this.handleChange}>
                        <option disabled selected value="">Select Day</option>
                        {
                            month&&year?days.map((x)=><option>{x}</option>):""
                        }
                    </select>
                </div>
                <div className="form-group">
                    <button className="btn btn-primary mb-4"
                    onClick={this.submitProd}>Submit</button><br/>
                    <button className="btn btn-primary"
                    onClick={()=>this.props.back2Home(0)}>Get Back to Home Page</button>
                </div>
                
            </React.Fragment>
        )
    }
}
export default ReceiveStocks;