import React,{Component} from 'react';
import AddProduct from './addProductForm';
import NavBar from './navbar';
import ReceiveStocks from './receiveStocks';
import ShowProd from './showProd';
class MainComp extends Component{
    state={
          products:[
              {code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",  specialOffer: false, limitedStock: false, quantity: 25  },
              {code: "MAGG021", price: 25, brand: "Nestle", category: "Food",  specialOffer: true, limitedStock: true, quantity: 10  },
              {code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",  specialOffer: true, limitedStock: true, quantity: 3  },
              {code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",  specialOffer: true, limitedStock: true, quantity: 5},
              {code: "MAGG451", price: 25, brand: "Nestle", category: "Food",  specialOffer: true, limitedStock: true, quantity: 0},
              {code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",  specialOffer: true, limitedStock: true, quantity: 5}
            ],
        view:0,
        editProdIndex:-1
    }
    changeView=(num)=>{
        let s1={...this.state};
        s1.view=num;
        this.setState(s1);
    }
    get2Edit=(index)=>{
        let s1={...this.state};
        s1.editProdIndex=index;
        s1.view=1;
        this.setState(s1);
    }
    handleSubmit=(product)=>{
        let s1={...this.state};
        s1.editProdIndex>=0?s1.products[s1.editProdIndex]=product:s1.products.push(product);
        s1.editProdIndex=-1;
        s1.view=0;
        this.setState(s1);
    }
    handleStockSubmit=(product)=>{
        let s1={...this.state};
        let index=s1.products.findIndex((x)=>x.code===product.code);
        s1.products[index].quantity+=parseInt(product.quantity);
        s1.products[index].date=product.date;
        s1.view=0;
        this.setState(s1);
    }
    render(){
        const product={code:"",price:"",brand:"",category:"",specialOffer:"",limitedStock:"",quantity:"",date:{year:"",month:"",day:""}};
        const brands =["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys", "P&G", "Colgate", "Parachute",
        "Gillete", "Dove", "Levis", "Van Heusen", "Manyavaar", "Zara"];
        const {products,view,editProdIndex} =this.state;
        let months=["January","February","March","April","May","June","July","August","September","October","November","December"];
        let productsCode=products.map((x)=>x.code);
        console.log(editProdIndex);
        let years=[];
        for(let i=1900;i<=2025;i++){
            years.push(i);
        }
        const categories=["Food","Personal Care","Apparel"];
        return(
            <div className="container">
                <NavBar
                products={products}/>
                {
                    view===0?(   
                        <React.Fragment><div className="row m-4">{products.map((x,index)=><ShowProd
                            product={x}
                            get2Edit={this.get2Edit}
                            index={index}/>)}</div><div>
                            <button className="btn btn-primary m-2"
                            onClick={()=>this.changeView(1)}>Add New Product</button>
                            <button className="btn btn-primary m-2"
                            onClick={()=>this.changeView(2)}>Receive Stocks</button>
                        </div></React.Fragment>
                    ):view===1?<AddProduct
                    product={editProdIndex>=0?products[editProdIndex]:product}
                    categories={categories}
                    brands={brands}
                    editIndex={editProdIndex}
                    onSubmit={this.handleSubmit}
                    back2Home={this.changeView}/>:<ReceiveStocks
                    codes={productsCode}
                    product={product}
                    handleStockSubmit={this.handleStockSubmit}
                    back2Home={this.changeView}
                    years={years}
                    months={months}/>
                }
                
            </div>
        )
    }
}
export default MainComp;